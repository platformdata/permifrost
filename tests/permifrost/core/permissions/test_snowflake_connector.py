from collections import namedtuple
from unittest.mock import call, Mock, MagicMock

import pytest
import os
import sqlalchemy


from permifrost.core.permissions.utils.snowflake_connector import SnowflakeConnector


@pytest.fixture
def snowflake_connector_env():
    os.environ["PERMISSION_BOT_USER"] = "TEST"
    os.environ["PERMISSION_BOT_PASSWORD"] = "TEST"
    os.environ["PERMISSION_BOT_ACCOUNT"] = "TEST"
    os.environ["PERMISSION_BOT_DATABASE"] = "TEST"
    os.environ["PERMISSION_BOT_ROLE"] = "TEST"
    os.environ["PERMISSION_BOT_WAREHOUSE"] = "TEST"


class TestSnowflakeConnector:
    def test_snowflaky(self):
        db1 = "analytics.schema.table"
        db2 = "1234raw.schema.table"
        db3 = '"123-with-quotes".schema.table'
        db4 = "1_db-9-RANDOM.schema.table"

        assert SnowflakeConnector.snowflaky(db1) == "analytics.schema.table"
        assert SnowflakeConnector.snowflaky(db2) == "1234raw.schema.table"
        assert SnowflakeConnector.snowflaky(db3) == '"123-with-quotes".schema.table'
        assert SnowflakeConnector.snowflaky(db4) == '"1_db-9-RANDOM".schema.table'

    def test_uses_oauth_if_available(self, mocker, snowflake_connector_env):
        mocker.patch("sqlalchemy.create_engine")
        os.environ["PERMISSION_BOT_OAUTH_TOKEN"] = "TEST"
        SnowflakeConnector()
        del os.environ["PERMISSION_BOT_OAUTH_TOKEN"]
        sqlalchemy.create_engine.assert_called_with(
            "snowflake://TEST:@TEST/?authenticator=oauth&token=TEST&warehouse=TEST"
        )

    def test_uses_key_pair_if_available(self, mocker, snowflake_connector_env):
        mocker.patch("sqlalchemy.create_engine")

        test_private_key = "TEST_PK"
        mocker.patch.object(
            SnowflakeConnector, "generate_private_key", return_value=test_private_key
        )

        os.environ["PERMISSION_BOT_KEY_PATH"] = "TEST"
        os.environ["PERMISSION_BOT_KEY_PASSPHRASE"] = "TEST"

        SnowflakeConnector()

        del os.environ["PERMISSION_BOT_KEY_PATH"]
        del os.environ["PERMISSION_BOT_KEY_PASSPHRASE"]

        sqlalchemy.create_engine.assert_called_with(
            "snowflake://TEST:@TEST/TEST?role=TEST&warehouse=TEST",
            connect_args={"private_key": test_private_key},
        )

    def test_uses_username_password_by_default(self, mocker, snowflake_connector_env):
        mocker.patch("sqlalchemy.create_engine")
        SnowflakeConnector()
        sqlalchemy.create_engine.assert_called_with(
            "snowflake://TEST:TEST@TEST/TEST?role=TEST&warehouse=TEST"
        )

    def test_run_query_executes_desired_query(self, mocker):
        mocker.patch("sqlalchemy.create_engine")
        conn = SnowflakeConnector()
        query = "MY FUN TESTING QUERY"

        conn.run_query(query)

        # Use argument matcher to match the query string in TextClause
        conn.engine.assert_has_calls([call.connect().__enter__().execute(mocker.ANY)])

        # Retrieve the actual call to verify the correct query
        actual_call = conn.engine.mock_calls[2]
        assert actual_call[1][0].text == query

    def test_run_query_returns_results(self, mocker):
        mocker.patch("sqlalchemy.create_engine")
        conn = SnowflakeConnector()
        expectedResult = "MY DATABASE RESULT"
        mocker.patch.object(
            conn.engine.connect().__enter__(), "execute", return_value=expectedResult
        )

        result = conn.run_query("query")

        assert result is expectedResult

    def test_get_current_user(self, mocker):
        mocker.patch("sqlalchemy.create_engine")
        conn = SnowflakeConnector()
        conn.run_query = mocker.MagicMock()
        # Create a mock result with the expected _mapping attribute
        mock_result = Mock()
        mock_result._mapping = {"user": "TEST_USER"}

        # Patch the fetchone method to return the mock result
        conn.run_query().fetchone.return_value = mock_result

        # Call the method under test
        user = conn.get_current_user()

        # Assert the expected result
        assert user == "test_user", f"Expected 'test_user', but got {user}"

    def test_get_current_role(self, mocker):
        # Patch the create_engine method
        mocker.patch("sqlalchemy.create_engine")

        # Initialize the connector
        conn = SnowflakeConnector()
        conn.run_query = MagicMock()

        # Create a mock result with the expected _mapping attribute
        mock_result = Mock()
        mock_result._mapping = {"role": "TEST_ROLE"}

        # Patch the fetchone method to return the mock result
        conn.run_query().fetchone.return_value = mock_result

        # Call the method under test
        role = conn.get_current_role()

        # Assert the expected result
        assert role == "test_role", f"Expected 'test_role', but got {role}"

    def test_show_roles(self, mocker):
        mocker.patch("sqlalchemy.create_engine")
        conn = SnowflakeConnector()
        conn.run_query = mocker.MagicMock()
        mock_result_1 = Mock()
        mock_result_1._mapping = {"name": "TEST_ROLE", "owner": "SUPERADMIN"}
        mock_result_2 = Mock()
        mock_result_2._mapping = {"name": "SUPERADMIN", "owner": "SUPERADMIN"}

        # Patch the fetchall method to return a list of mock results
        conn.run_query().fetchall.return_value = [mock_result_1, mock_result_2]

        # Call the method under test
        roles = conn.show_roles()

        # Assert the expected result
        expected_roles = {"test_role": "superadmin", "superadmin": "superadmin"}
        assert roles == expected_roles, f"Expected {expected_roles}, but got {roles}"
